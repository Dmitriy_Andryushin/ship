import java.util.ArrayList;

public class Ship{
    public static final String SHOT_HERE = "Вы уже стреляли в эту точку";
    public static final String GOT = "Вы попали!";
    public static final String SLIP = "Вы промахнуллись";
    public static final String GAME_OVER = "Корабль потоплен, игра окончена";
    private ArrayList coordinateShip = new ArrayList();
    private ArrayList coordinateShot = new ArrayList();
    
    /**
    * Метод, который рандомно размещает кораблик
    */
    
    public Ship (int channelLength){
        int a = (int) (Math.random()*(channelLength-2));
        coordinateShip.add(a);
        coordinateShip.add(a+1);
        coordinateShip.add(a+2);

    }

    public Ship (){
        coordinateShip.add(0);
        coordinateShip.add(1);
        coordinateShip.add(2);
    }
    
    /**
    * Метод, который проверяет стрелял ли пользователь в эту точку,
    * Есть ли в этой точки кораблик,
    * Попал или не попал пользователь в кораблик
    */


    public boolean isDestroyed(int shot) {
        if(coordinateShot.contains(shot)){
            System.out.println(SHOT_HERE);
        }else{
            if (!coordinateShip.contains(shot)){
                System.out.println(SLIP);
                coordinateShot.add(shot);
            }else{
                int x = coordinateShip.indexOf(shot);
                coordinateShip.remove(x);
                coordinateShot.add(shot);
                System.out.println(GOT);
            }
        }
        if (coordinateShip.isEmpty()) {
            System.out.println(GAME_OVER);
        }
        return coordinateShip.isEmpty();
    }

}