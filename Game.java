import java.util.Scanner;
/**
* Главный класс позволяющий поиграть и потопить вражеский корабль
*
* @author Андрюшин Дмитрий 16ИТ18к
*/
public class Game {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args){
        System.out.println("Введите размер канала");
        int channelLength = scanner.nextInt();
        while(channelLength < 2 ){
            System.out.println("Вы ввели размер канала куда не помещается корабль, попробуйте ввести размер заного");
            channel = scanner.nextInt();
        }
        Ship ship = new Ship (channelLength);
        System.out.println("Вы потопили корабль за " + countAndShooting(channelLength, ship) + " попыток." + " Спасибо за игру");

    }
    
    /**
    * Метод, который считает количество выстрелов и реализует стрельбу
    *
    * @return количество выстрелов
    */
    
    public static int countAndShooting (int channelLength, Ship ship) {
        boolean result;
        int count = 0;
        do {
            System.out.println("Введите координату места куда будете стрелять" );
            int shot = scanner.nextInt();
            while(shot < 0 && shot >= channelLength){
                System.out.println("Введите число которое, будет больше 0 и меньше или равно длине канала ");
                shot = scanner.nextInt();
            }
            result = ship.isDestroyed(shot);
            count++;
        } while (result != true);
        return count;
    }
}

